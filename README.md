Linear Models Framework
=======================

This repository contains templates for UW-Stout's MSCS-335/535 (Machine Learning).

The code is a self-contained Python module, that depends on NumPy, SciPy, and SKLearn.
It includes the (incomplete) classes:
 - LinearModel
 - LassoLars

You will want to download the package and place it somewhere where it can be loaded by a Jupyter notebook running Python3.
If you are familiar with Git, just clone it.
If nt, download this zipfile and unpack it:
https://gitlab.com/abrahamdavidsmith/mscs_linear/repository/master/archive.zip 
