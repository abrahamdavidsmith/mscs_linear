r"""This module contains regressors based on the SciKitLearn framework.

    - :class:`LinearModel`, for basic Gauss--Markov linear regression
    - :class:`LassoLars`, for running the Lasso/LARS algorithm to find a
      trajectory of L2/L1-opimitizing regressions.

Copyright
---------
- This file is part of https://github.com/abrahamdavidsmith/mscs_linear/ 
- copyright, 2018 by Abraham D. Smith. 
- CC0 license.  See https://creativecommons.org/choose/zero/
"""

from sklearn.base import BaseEstimator, RegressorMixin
import scipy.linalg as la
import numpy as np

class LinearModel(BaseEstimator, RegressorMixin):
    r"""We are making a class (Object-oriented style) in Python, that inherits the 
     general frameworks from SciKitLearn. 
     
     The required methods for a SciKitLearn-compatible class are
      - fit(x, y)  Fit the model using X, y as training data.
      - get_params()  Get parameters for this estimator.
      - predict(x)  Predict output y using input X the linear model
      - score(x, y)  Returns the FVE (that is, R^2) of the prediction for testing data X,Y.
     
     Some parameters of the model are set via __init__ 

     Examples
     --------
     >>> import numpy as np
     >>> train_x = np.array([[2, 2], [-1, 3], [1, 1], [2, 0]], dtype='float64')
     >>> train_y = np.array([[5], [3], [2], [4]],dtype='float64')
     >>> m = LinearModel(normalize=True)
     >>> m.fit(train_x,train_y)
     We fit 2 predictors using 4 samples.  The model is
     [ 1.173...  0.857...]
     >>> test_x = np.array([[1, 1],[2, 2.1]], dtype='float64')
     >>> test_y = np.array([[3.1],[5.1]], dtype='float64')
     >>> m.predict(test_x).flatten()
     array([ 3.07...,  5.08...])
     >>> m.score(test_x, test_y)
     0.999...
   """

    def __init__(self, normalize=True, fit_intercept=False):
        r""" Create a LinearModel.

        Parameters
        ----------
         - normalize    `bool`  Re-normalize the data before fitting. Default: True
         - fit_intercept   `bool`  Allow an intercept in the model.  Default: False 

        Notes
        -----
        normalize and fit_intercept interact in ways that you'll have to think
        about carefully!

        """

        # Just initialize some data that is set by `self.fit`
        self.model = None
        self.covar_xx = None
        self.covar_xy = None
        self.covar_yy = None
        self.mean_x = None
        self.mean_y = None
        self.std_x = None
        self.std_y = None
        self.num_predictors = None
        self.num_samples = None
        self.normalize = normalize
        self.fit_intercept = fit_intercept
        pass

    def fit(self, x, y):
        """ Fit a linear model to this training data.  This is the step that 
        runs the actual estimator.  The model is stored in the `self.model`
        attribute.

        Parameters
        ----------
        x   - an N-by-p `numpy.ndarray` of training predictors
        y   - an N-by-1 `numpy ndarray` of training responses

        """
        assert x.shape[0] == y.shape[0], "Sample sizes of Predictor X and Reponse Y do not match!"
        
        # Make a copy, so we can manipulate them safely.
        X = x.copy()
        Y = y.copy() 

        # Make sure Y is a column.
        Y.shape = (Y.shape[0], 1)

        self.num_predictors = X.shape[1]
        self.num_samples = X.shape[0]

        if self.fit_intercept:
            raise NotImplementedError("""You should add code to deal with the
            case where the user wants to allow a constant term in the model!
            Think about how this would interact with the `normalize` option,
            too.""")

        if self.normalize:
            self.mean_x = X.mean(axis=0)
            self.std_x = X.std(axis=0)
            X = (X - self.mean_x)/self.std_x
            self.mean_y = Y.mean(axis=0)
            self.std_y = Y.std(axis=0)
            Y = (Y - self.mean_y)/self.std_y

        self.covar_xx = X.T.dot(X)
        self.covar_xy = X.T.dot(Y)
        self.covar_yy = Y.T.dot(Y)
       
        # Here is the actual solving.
        self.model = la.solve(self.covar_xx, self.covar_xy)
        
        assert self.model.shape == (self.num_predictors,1)


        print(f"We fit {self.num_predictors} predictors using {self.num_samples} samples.  The model is\n{self.model.flatten()}")
        pass 
            
    def get_params(self):
        r""" Provide dictionary of fit parameters to the user. """
        return { 'model': self.model,
                 'covar_XX': self.covar_xx,
                 'covar_XY': self.covar_xy,
                 'covar_YY': self.covar_yy,
                 'num_predictors': self.num_predictors,
                 'num_samples': self.num_samples}
    
    def predict(self, x):
        r""" Apply the model to the testing data X. 
        
        Parameters
        ----------
        x   - an N-by-p `numpy.ndarray` of testing predictors
        
        Returns
        -------
        y  - an N-by-1 `numpy.ndarray` of responses.
        """ 
        if self.normalize:
            # Apply the normalization shift.
            X = (x - self.mean_x) / self.std_x
            Y = X.dot(self.model)
            y = (Y * self.std_y) + self.mean_y
        else:
            y = x.dot(self.model)
 
        return y 

    def residual(self, x, y):
        r""" Compute the residual vector for testing data x,y 
       
        Parameters
        ----------
         - x   an N-by-p `numpy.ndarray` of testing predictors
         - y   an N-by-1 `numpy.ndarray` of testing responses.
        """
        return y - self.predict(x)

    def score(self, x, y):
        r""" Compute fraction-of-variance-explained (FVE or R^2) of the model
        on this testing data.  
        
        Notes
        -----
        If the data was not normalized, then the variance of y might
        be misunderstood.   If you want to fix this, then play with the
        normalization methods.
       
        Parameters
        ----------
         - x   an N-by-p `numpy.ndarray` of testing predictors
         - y    an N-by-1 `numpy.ndarray` of testing responses.
        """
        r = self.residual(x,y)
        return 1. - (r.T.dot(r))[0,0]/self.covar_yy[0,0]


class LassoLars(BaseEstimator, RegressorMixin):
    r"""We are making a class (Object-oriented style) in Python, that inherits the 
     general frameworks from SciKitLearn. 
     
     The required methods for a SciKitLearn-compatible class are
      - fit(x, y)  Fit the model using X, y as training data.
      - get_params()  Get parameters for this estimator.
      - predict(x)  Predict output y using input X the linear model
      - score(x, y)  Returns the FVE (that is, R^2) of the prediction for testing data X,Y.
     
     Some parameters of the model are set via __init__ 

     For Examples, compare to the official version at
     http://scikit-learn.org/stable/modules/linear_model.html#lars-lasso
    """ 

    def __init__(self, normalize=True, fit_intercept=False, min_fve=1.0, max_vars=np.inf):
        r""" Create a Lasso/LARS model trajectory.

        Parameters
        ----------
         - normalize    `bool`  Re-normalize the data before fitting. Default: True
         - fit_intercept   `bool`  Allow an intercept in the model.  Default: False 

        Notes
        -----
        normalize and fit_intercept interact in ways that you'll have to think
        about carefully!

        When producing the Lasso/LARS trajectory, you can stop early if either
        min_fve or max_vars is achieved.
        """

        # Just initialize some data that is set by and used by `self.fit`
        self.model = None
        self.covar_xx = None
        self.covar_xy = None
        self.covar_yy = None
        self.mean_x = None
        self.mean_y = None
        self.std_x = None
        self.std_y = None
        self.num_predictors = None
        self.num_samples = None
        self.normalize = normalize
        self.fit_intercept = fit_intercept
        self.use_model = -1
        self.min_fve = min_fve
        self.max_vars = max_vars
        pass

    def fit(self, x, y):
        """ Fit a *family* of linear models to this training data.  This is the step that 
        runs the actual LARS/Lasso algorithm estimator.  
        
        The models are stored in the `self.model` attribute, which is a
        p-by-k :class:`numpy.ndarray`.  Each column is a model.  The 0th column
        is the first 1-variable model found by LARS/Lasso, and the (k-1)th
        column is the last model produced.  The last model produced depends on
        the parameters given to :func:`__init__`.  Typically, the last model is
        the complete model.

        Parameters
        ----------
        x   - an N-by-p `numpy.ndarray` of training predictors
        y   - an N-by-1 `numpy ndarray` of training responses

        """
        
        # the main algrithm goes here (but feel free to break out smaller
        # functions if that helps your code organization).
        raise NotImplementedError("Dear Student, You have to write this part!")
            
    def get_params(self):
        r""" Provide dictionary of fit parameters to the user. """
        # Make a dictionary of the parameters given and created, for
        # ease-of-use by future users.
        raise NotImplementedError("Dear Student, You have to write this part!")

    def set_params(self, **arguments):
        r""" Change tuning parameters after a model is built.  In particular,
        `lars.set_params(use_model=i)` means to use ith column of self.model for
        predictions.
        """
        raise NotImplementedError("Dear Student, You have to write this part!")
         

    def predict(self, x):
        r""" Apply the model to the testing data X.  This uses the model in the
        column of `self.model` chosen by the parameter `lars.use_model`, which
        defaults to -1, the last model made.
        
        Parameters
        ----------
        x   - an N-by-p `numpy.ndarray` of testing predictors
        
        Returns
        -------
        y  - an N-by-1 `numpy.ndarray` of responses.
        """ 
        #         
        raise NotImplementedError("Dear Student, You have to write this part!")

    def residual(self, x, y):
        r""" Compute the residual vector for testing data x,y 
        
        Parameters
        ----------
         - x   an N-by-p `numpy.ndarray` of testing predictors
         - y    an N-by-1 `numpy.ndarray` of testing responses.
        """
        return y - self.predict(x)

    def score(self, x, y):
        r""" Compute fraction-of-variance-explained (FVE or R^2) of the model
        on this testing data.  
        
        Notes
        -----
        If the data was not normalized, then the variance of y might
        be misunderstood.   If you want to fix this, then play with the
        normalization methods.
       
        Parameters
        ----------
         - x   an N-by-p `numpy.ndarray` of testing predictors
         - y    an N-by-1 `numpy.ndarray` of testing responses.
        """
        
        r = self.residual(x,y)
        return 1. - (r.T.dot(r))[0,0]/self.covar_yy[0,0]

    def plot_coeffs(self, axes):
        r""" Given a "axes" object from MatPlotLib, produce a model path plot
        like the one seen here:
        http://scikit-learn.org/stable/auto_examples/linear_model/plot_lasso_lars.html

        Returns
        -------
        nothing.  simply plots on the given axes.
        """
        raise NotImplementedError("Dear Student, You have to write this part!")

    def plot_fve(self, canvas, test_x=None, test_y=None):
        r""" Given a "axes" object from MatPlotLib, produce a plot of FVE
        versus the model path.
        So, the horizontal axis should be the same as `plot_coeffs`, but the
        plot should have an (increasing) graph of FVE.

        Parameters
        ----------
         - axes    an axis to plot on, as produced by `matplotlib.pyplot.subplots()`
         - test_x  a testing set of predictors to use for FVE 
                   (Default: None, use original training data)
         - test_y  a testing set of responses to use for FVE 
                   (Default: None, use original training data)

        Returns
        -------
        nothing.  simply plots on the given axes.
        """
        raise NotImplementedError("Dear Student, You have to write this part!")

